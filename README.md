# polymer

An adhesive layer between a system generating events and Kafka

## Overview

![overview](./images/polymer.png)

* [polymer](https://gitlab.com/jstone28/polymer) - An adhesive layer between a system generating events and kafka
* [instrumentation](https://gitlab.com/jstone28/instrumentation) - An event generating system

## Getting Started

Start [Zookeeper]() and [Kafka]() from `brew services`, and open a kafka consumer to watch for messages being sent from the topic `simple-message-topic`

```bash
./scripts/startup
```

Run polymer with the following command:

```bash
./gradlew bootRun
```

Next, Clone [instrumentation](https://gitlab.com/jstone28/instrumentation):

```bash
cd instrumentation/ && make run
```

Verify messages on the topic `simple-message-topic` are displayed

![messages](./images/kafka-consumer.png)
