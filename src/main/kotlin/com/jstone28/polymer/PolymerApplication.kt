package com.jstone28.polymer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PolymerApplication

fun main(args: Array<String>) {
	runApplication<PolymerApplication>(*args)
}
